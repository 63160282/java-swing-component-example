/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author User
 */
public class SwingExample {

    public static void main(String[] args) {
        JFrame f = new JFrame();
        JButton b = new JButton("click");

        b.setBounds(100, 100, 100, 40);

        f.add(b);

        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }
}
